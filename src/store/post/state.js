export default {
  // 
  state:{
    posts: [],
    posts_meta:{
      path: null,
      current_page: null,
      next_page_url: null,
      last_page: null,
      total: null,
      per_page: null
    },
    mentors: [],
    inserted_post_id: null
  },
}
