
// import { $axios  } from 'plugins/axios'
import axios from 'axios'

export function insert_post ({commit}, data) {
    return new Promise((resolve, reject) => {
        axios.post("/api/post", data)
        .then(response => {
            if(response.data.status == 'successful'){
                commit('inserted_post_id', {data: response.data.data})
                resolve(response);
            }else if(response.data.status == 'error'){
                return
            }else{
                return
            }
        })
        .catch(err => {           
                reject(err);
            }
        )    
    })
}


export function get_posts ({commit}) {
    return new Promise((resolve, reject) => {
        axios.get("/api/post")
        .then(response => {
            if(response.data.status == 'successful'){
                commit('get_posts', {data: response.data.data})
                resolve(response);
            }else if(response.data.status == 'error'){
                this.$q.notify(response.data.message)
                return
            }else{
                this.$q.notify('somthing went wrong. Please try again')
                return
            }
        })
        .catch(err => {           
                reject(err);
            }
        )    
    })
}

export function load_more_posts ({commit}, page) {
    return new Promise((resolve, reject) => {
        axios.get('/api/post?page='+page)
        .then(response => {
            if(response.data.status == 'successful'){
                commit('load_more_posts', {data: response.data.data})
                resolve(response);
            }else if(response.data.status == 'error'){
                return
            }else{
                return
            }
        })
        .catch(err => {           
                reject(err);
            }
        )    
    })
}


export function get_user_posts ({commit}, slug) {
    return new Promise((resolve, reject) => {
        axios.get("/api/post/"+slug)
        .then(response => {
            if(response.data.status == 'successful'){
                commit('get_posts', {data: response.data.data})
                resolve(response);
            }else if(response.data.status == 'error'){
                this.$q.notify(response.data.message)
                return
            }else{
                this.$q.notify('somthing went wrong. Please try again')
                return
            }
        })
        .catch(err => {           
                reject(err);
            }
        )    
    })
}



export function get_unread_message ({commit}, slug) {
    return new Promise((resolve, reject) => {
        axios.get("/api/post/"+slug)
        .then(response => {
            if(response.data.status == 'successful'){
                commit('get_posts', {data: response.data.data})
                resolve(response);
            }else if(response.data.status == 'error'){
                this.$q.notify(response.data.message)
                return
            }else{
                this.$q.notify('somthing went wrong. Please try again')
                return
            }
        })
        .catch(err => {           
                reject(err);
            }
        )    
    })
}

export function get_mentors ({commit}) {
    return new Promise((resolve, reject) => {
        axios.get("/api/mentor")
        .then(response => {
            // console.log(response.data.data);
            if(response.data.status == 'successful'){
                commit('get_mentors', {data: response.data.data})
                resolve(response);
            }else if(response.data.status == 'error'){
                this.$q.notify(response.data.message)
                return
            }else{
                this.$q.notify('somthing went wrong. Please try again')
                return
            }
        })
        .catch(err => {           
                reject(err);
            }
        )    
    })
}



export function hide_post ({commit}, id) {
    commit('hide_post', id);
}