// pass user infomation to store state after successful feach from api
export function get_posts (state, data) {
    state.state.posts = data.data.data
    state.state.posts_meta.path = data.data.path
    state.state.posts_meta.current_page = data.data.current_page
    state.state.posts_meta.next_page_url = data.data.next_page_url
    state.state.posts_meta.last_page = data.data.last_page
    state.state.posts_meta.total = data.data.total
    state.state.posts_meta.per_page = data.data.per_page
}

export function inserted_post_id (state, data) {
    state.state.inserted_post_id = data.data
}

export function load_more_posts (state, data) {
    for(var i = 0; i < data.data.data.length; i++){
        state.state.posts.push(data.data.data[i])
    }    
    state.state.posts_meta.path = data.data.path
    state.state.posts_meta.current_page = data.data.current_page
    state.state.posts_meta.next_page_url = data.data.next_page_url
    state.state.posts_meta.last_page = data.data.last_page
    state.state.posts_meta.total = data.data.total
    state.state.posts_meta.per_page = data.data.per_page
}

// pass user infomation to store state after successful feach from api
export const auth_user_success = (state, data) => {
    state.state.user = data.data;
}

// get list of surgested mentors from api
export function get_mentors (state, data) {
    state.state.mentors = data
}

// hide post
export function hide_post (state, id) {
    let index = state.state.posts.findIndex( (post) => {
        return post.id == id
    });

    if( index !== -1 ) {
        state.state.posts.splice(index, 1);
    }
}