
// import { $axios  } from 'plugins/axios'
import axios from 'axios'




export function save_thought ({commit}, data) {
    return new Promise((resolve, reject) => {
        axios.post("/api/afterThought", data)
    .then(response => {
        if(response.data.status == 'successful'){
            commit('save_thought', {data: response.data.data})
            resolve(response);
        }else if(response.data.status == 'error'){
            this.$q.notify(response.data.message)
            return
        }else{
            this.$q.notify('somthing went wrong. Please try again')
            return
        }
      })
    .catch(err => {           
            reject(err);
        }
    )    
})
}


export function get_subscriptions ({commit}, id) {
    return new Promise((resolve, reject) => {
        axios.get("/api/subscription/"+id)
        .then(response => {
            if(response.data.status == 'successful'){
                commit('get_subscriptions', {data: response.data.data})
                resolve(response);
            }else if(response.data.status == 'error'){
                this.$q.notify(response.data.message)
                return
            }else{
                this.$q.notify('somthing went wrong. Please try again')
                return
            }
        })
        .catch(err => {           
                reject(err);
            }
        )    
    })
}

export function get_subscribers ({commit}, id) {
    return new Promise((resolve, reject) => {
        axios.get("/api/subscribers/"+id)
        .then(response => {
            if(response.data.status == 'successful'){
                commit('get_subscribers', {data: response.data.data})
                resolve(response);
            }else if(response.data.status == 'error'){
                this.$q.notify(response.data.message)
                return
            }else{
                this.$q.notify('somthing went wrong. Please try again')
                return
            }
        })
        .catch(err => {           
                reject(err);
            }
        )    
    })
}


export function get_subscription_category ({commit}) {
    return new Promise((resolve, reject) => {
        axios.get("/api/subscription/category")
        .then(response => {
            if(response.data.status == 'successful'){
                commit('get_subscription_category', {data: response.data.data})
                resolve(response);
            }else if(response.data.status == 'error'){
                this.$q.notify(response.data.message)
                return
            }else{
                this.$q.notify('somthing went wrong. Please try again')
                return
            }
        })
        .catch(err => {           
                reject(err);
            }
        )    
    })
}
