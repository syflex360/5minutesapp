
// import { $axios  } from 'plugins/axios'
import axios from 'axios'


export function get_categories ({commit}) {
    return new Promise((resolve, reject) => {
        axios.get("/api/get/categories")
        .then(response => {
            commit('get_categories', {data: response.data})
            resolve(response);
        })
        .catch(err => {           
                reject(err);
            }
        )    
    })
}
