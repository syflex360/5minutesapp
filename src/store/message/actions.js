
// import { $axios  } from 'plugins/axios'
import axios from 'axios'




export function get_contact ({commit}) {
    return new Promise((resolve, reject) => {
     axios.get('api/message')
    .then(response => {
        if(response.data.status == 'successful'){
            commit('get_contact', {data: response.data.data})
            resolve(response);
        }else if(response.data.status == 'error'){
            return
        }else{
            return
        }
      })
    .catch(err => {
            reject(err);
        }
    )    
})
}


export function get_message ({commit}, id) {
    return new Promise((resolve, reject) => {
        axios.get("/api/message/"+id)
        .then(response => {
            if(response.data.status == 'successful'){
                commit('get_message', {data: response.data.data})
                resolve(response);
            }else if(response.data.status == 'error'){
                return
            }else{
                return
            }
        })
        .catch(err => {           
                reject(err);
            }
        )    
    })
}


export function save_message ({commit}, form) {
    return new Promise((resolve, reject) => {
        axios.post("/api/message", form)
        .then(response => {
            if(response.data.status == 'successful'){
                commit('save_message', {data: response.data.data})
                resolve(response);
            }else if(response.data.status == 'error'){
                return
            }else{
                return
            }
        })
        .catch(err => {           
                reject(err);
            }
        )    
    })
}
