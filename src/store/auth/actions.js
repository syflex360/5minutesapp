import axios from 'axios'
import { LocalStorage } from 'quasar'

export async function authAction ({commit}, data) {
   await commit('auth_success', data)
}


export function auth_register ({commit}, data) {
    return new Promise((resolve, reject) => {
    axios.post("/api/register", data)
    .then(response => {
        if(response.data.status == 'successful'){
            commit('auth_registered', {data: response.data.data})
            resolve(response);
        }else{
            resolve(response);
            return
        }
      })
    .catch(err => {         
            reject(err);
        }
    )    
})
}

export function auth_login ({commit}, data) {
    return new Promise((resolve, reject) => {
        axios.post("/api/login", data)
    .then(response => {
        if(response.data.status == 'successful'){
            setAxiosHeaders(response.data.token)
            commit('auth_login', {data: response.data.data, token: response.data.token})
            resolve(response);
        }else{
            resolve(response);
            return
        }
      })
    .catch(err => {     
            reject(err);
        }
    )    
})
}

// export function auth_social_login ({commit}, token) {
//     return new Promise((resolve, reject) => {
//         commit('auth_social_login', {data: token})
//     //   axios.get("/api/social/"+ data)
//     // .then(response => {
//     //     console.log(response.data)
//     //     // if(response.data.status == 'successful'){
//     //     //     setAxiosHeaders(response.data.token)
//     //     //     commit('auth_login', {data: response.data.data, token: response.data.token})
//     //     //     resolve(response);
//     //     // }else if(response.data.status == 'error'){
//     //     //     this.$q.notify(response.data.message)
//     //     //     return
//     //     // }else{
//     //     //     this.$q.notify('somthing went wrong. Please try again')
//     //     //     return
//     //     // }
//     //   })
//     .catch(err => {           
//             reject(err);
//         }
//     )    
// })
// }

export  async function auth_user ({commit}) {
    return await new Promise((resolve, reject) => {
        axios.get("/api/token/user")
    .then(response => {
        if(response.data.status == 'successful'){
            commit('auth_user', {data: response.data.data});
            resolve(response);
        } 
      })
    .catch(err => {           
            reject(err);
        }
    )    
})
}

export  async function user_profile ({commit}, data) {
    return await new Promise((resolve, reject) => {
        axios.get("/api/user/"+data)
    .then(response => {
        if(response.data.status == 'successful'){
            commit('user_profile', {data: response.data.data});
            resolve(response);
        } 
      })
    .catch(err => {           
            reject(err);
        }
    )    
})
}

export async function auth_logout ({commit}){
    return await new Promise((resolve, reject) => {
    axios.get("/api/token/logout")
    .then(response => {
        commit('auth_user_logout');
        resolve(response);
      })
    .catch(err => { 
            commit('auth_user_logout');     
            reject(err);
        }
    )    
})
}



function setAxiosHeaders (token) {
    axios.defaults.headers.common['Authorization'] = token || LocalStorage.get.item('5minutes-token') || state.token || ''
  }
