import { LocalStorage } from 'quasar'
// export function Mutation (state) {
// }

// commit changes after a successful login
export const auth_login = (state, data) => {
    LocalStorage.set('5minutes-token', data.token);
    state.state.token = data.token;
    state.state.user = data.data;
}

// commit just registered user
export const auth_registered = (state, data) => {
    state.state.registered_user = data;
}

export const auth_social_login = (state, data) => {
    LocalStorage.set('5minutes-token', data.data);
    state.state.token = data.token;
    state.state.user = data.data;
}

// pass user infomation to store state after successful feach from api
export const auth_user = (state, data) => {
    state.state.user = data.data;
}

// pass user infomation to store state after successful feach from api
export const user_profile = (state, data) => {
    state.state.profile = data.data;
}

// log user out of system 
export const auth_user_logout = (state) => {
    LocalStorage.remove('5minutes-token')
    state.state.token = '';
    state.state.user = null;
}
