// pass user infomation to store state after successful feach from api
export function get_contact (state, data) {
    state.state.contact = data.data
}

export function get_message (state, data) {
    state.state.message = data.data
}

export function save_message (state, data) {
    state.state.message.push(data.data)
}

// get list of surgested mentors from api
export function selected (state, data) {
    state.state.selected = data
}