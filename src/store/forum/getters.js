export function forums (state) {
    return  state.state.forums
}

export function conversation (state) {
    return  state.state.conversation
}

export function users (state) {
    return  state.state.users
}
