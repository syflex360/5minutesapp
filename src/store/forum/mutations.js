// pass user infomation to store state after successful feach from api
export function get_forums (state, data) {
    state.state.forums = data.data
}

export function get_forum_conversation (state, data) {
    state.state.conversation = data.data
}

export function get_forum_users (state, data) {
    state.state.users = data.data
}


export function save_conversation (state, data) {
    state.state.conversation.push(data.data)
}
