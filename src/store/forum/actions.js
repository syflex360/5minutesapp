
// import { $axios  } from 'plugins/axios'
import axios from 'axios'




export function get_forums ({commit}) {
    return new Promise((resolve, reject) => {
     axios.get('api/forum')
    .then(response => {
        if(response.data.status == 'successful'){
            commit('get_forums', {data: response.data.data})
            resolve(response);
        }else if(response.data.status == 'error'){
            return
        }else{
            return
        }
      })
    .catch(err => {
            reject(err);
        }
    )    
})
}


export function get_forum_conversation ({commit}, id) {
    return new Promise((resolve, reject) => {
        axios.get("/api/forum_conversation/"+id)
        .then(response => {
            if(response.data.status == 'successful'){
                commit('get_forum_conversation', {data: response.data.data})
                resolve(response);
            }else if(response.data.status == 'error'){
                return
            }else{
                return
            }
        })
        .catch(err => {           
                reject(err);
            }
        )    
    })
}

export function get_forum_users ({commit}, id) {
    return new Promise((resolve, reject) => {
        axios.get("/api/forum_users/"+id)
        .then(response => {
            if(response.data.status == 'successful'){
                commit('get_forum_users', {data: response.data.data})
                resolve(response);
            }else if(response.data.status == 'error'){
                return
            }else{
                return
            }
        })
        .catch(err => {           
                reject(err);
            }
        )    
    })
}


export function save_conversation ({commit}, form) {
    return new Promise((resolve, reject) => {
        axios.post("/api/forum_conversation", form)
        .then(response => {
            if(response.data.status == 'successful'){
                commit('save_conversation', {data: response.data.data})
                resolve(response);
            }else if(response.data.status == 'error'){
                return
            }else{
                return
            }
        })
        .catch(err => {           
                reject(err);
            }
        )    
    })
}
