
// import { $axios  } from 'plugins/axios'
import axios from 'axios'




export function save_note ({commit}, data) {
    return new Promise((resolve, reject) => {
        axios.post("/api/note", data)
    .then(response => {
        if(response.data.status == 'successful'){
            commit('save_note', {data: response.data.data})
            resolve(response);
        }else if(response.data.status == 'error'){
            this.$q.notify(response.data.message)
            return
        }else{
            this.$q.notify('somthing went wrong. Please try again')
            return
        }
      })
    .catch(err => {           
            reject(err);
        }
    )    
})
}


export function get_notes ({commit}) {
    return new Promise((resolve, reject) => {
        axios.get("/api/post")
        .then(response => {
            if(response.data.status == 'successful'){
                commit('get_posts', {data: response.data.data})
                resolve(response);
            }else if(response.data.status == 'error'){
                this.$q.notify(response.data.message)
                return
            }else{
                this.$q.notify('somthing went wrong. Please try again')
                return
            }
        })
        .catch(err => {           
                reject(err);
            }
        )    
    })
}
