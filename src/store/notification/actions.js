
// import { $axios  } from 'plugins/axios'
import axios from 'axios'


export function get_notifications ({commit}) {
    return new Promise((resolve, reject) => {
        axios.get("/api/notification")
        .then(response => {
            if(response.data.status == 'successful'){
                commit('get_notifications', {message: response.data.messagge, notification: response.data.notification})
                resolve(response);
            }else if(response.data.status == 'error'){
                return
            }else{
                this.$q.notify('somthing went wrong. Please try again')
                return
            }
        })
        .catch(err => {           
                reject(err);
            }
        )    
    })
}


export function save_thought ({commit}, data) {
    return new Promise((resolve, reject) => {
        axios.post("/api/afterThought", data)
    .then(response => {
        if(response.data.status == 'successful'){
            commit('save_thought', {data: response.data.data})
            resolve(response);
        }else if(response.data.status == 'error'){
            this.$q.notify(response.data.message)
            return
        }else{
            this.$q.notify('somthing went wrong. Please try again')
            return
        }
      })
    .catch(err => {           
            reject(err);
        }
    )    
})
}

