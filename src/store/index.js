import Vue from 'vue'
import Vuex from 'vuex'

import auth from './auth'
import post from './post'
import note from './note'
import thought from "./thought";
import achievement from './achievement'
import subscription from './subscription'
import notification from './notification'
import chat from './chat'
import message from './message'
import forum from './forum'
import category from './categories'

Vue.use(Vuex)

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation
 */

export default function (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: {
      auth,
      post,
      note,
      thought,
      achievement,
      subscription,
      notification,
      chat,
      message,
      forum,
      category,
    }
  })

  return Store
}
