
// import { $axios  } from 'plugins/axios'
import axios from 'axios'




export function save_achievement ({commit}, data) {
    return new Promise((resolve, reject) => {
        axios.post("/api/achievement", data)
    .then(response => {
        if(response.data.status == 'successful'){
            commit('save_achievement', {data: response.data.data})
            resolve(response);
        }else if(response.data.status == 'error'){
            this.$q.notify(response.data.message)
            return
        }else{
            this.$q.notify('somthing went wrong. Please try again')
            return
        }
      })
    .catch(err => {           
            reject(err);
        }
    )    
})
}


export function update_achievement ({commit}, data) {
    return new Promise((resolve, reject) => {
    axios.patch("/api/achievement/"+ data.id, data)
    .then(response => {
        if(response.data.status == 'successful'){
            commit('update_achievement', {data: response.data.data})
            resolve(response);
        }else if(response.data.status == 'error'){
            this.$q.notify(response.data.message)
            return
        }else{
            this.$q.notify('somthing went wrong. Please try again')
            return
        }
      })
    .catch(err => {           
            reject(err);
        }
    )    
})
}

export function make_default_achievement ({commit}, data) {
    return new Promise((resolve, reject) => {
    axios.patch("/api/achievement/"+ data.id, data)
    .then(response => {
        if(response.data.status == 'successful'){
            commit('make_default_achievement', {data: response.data.data})
            resolve(response);
        }else if(response.data.status == 'error'){
            this.$q.notify(response.data.message)
            return
        }else{
            this.$q.notify('somthing went wrong. Please try again')
            return
        }
      })
    .catch(err => {           
            reject(err);
        }
    )    
})
}


export function get_achievements ({commit}, id) {
    return new Promise((resolve, reject) => {
        axios.get("/api/achievement/"+id)
        .then(response => {
            if(response.data.status == 'successful'){
                commit('get_achievements', {data: response.data.data})
                resolve(response);
            }else if(response.data.status == 'error'){
                this.$q.notify(response.data.message)
                return
            }else{
                this.$q.notify('somthing went wrong. Please try again')
                return
            }
        })
        .catch(err => {           
                reject(err);
            }
        )    
    })
}


export function delete_achievement ({commit}, data) {
    return new Promise((resolve, reject) => {
    axios.delete("/api/achievement/"+ data)
    .then(response => {
        if(response.data.status == 'successful'){
            commit('delete_achievement', {data: data})
            resolve(response);
        }else if(response.data.status == 'error'){
            this.$q.notify(response.data.message)
            return
        }else{
            this.$q.notify('somthing went wrong. Please try again')
            return
        }
      })
    .catch(err => {           
            reject(err);
        }
    )    
})
}