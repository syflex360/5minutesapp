
const routes = [
  {
    path: '/',
    component: () => import('layouts/AppLayout.vue'),
    children: [
      { name: 'Home', path: '', component: () => import('pages/Index.vue') },
      { name: 'Auth', path: 'auth', component: () => import('pages/Auth.vue'),
        children: [      
          { name: 'Login', path: 'login' },      
          { name: 'Register', path: 'register', 
            children: [      
              { name: 'Mentor', path: 'mentor',  },      
              { name: 'Organization', path: 'organization', },
              { name: 'Registration-Success', props: true, path: 'success'},
              { name: 'Activate', props: true, path: 'activate'},
            ]
          },
          { name: 'Forgot-Password', path: 'forgot-password'},
          { name: 'Reset-Password', path: 'reset-password'},
          { name: 'Activate-Account', props: true, path: 'activate/account/:token'},
        ]
      },       
      { name: 'Social', path: ':provider/:callback', component: () => import('pages/Social.vue') },
      { name: 'Feed', path: 'feed', component: () => import('pages/Article.vue'), meta: {requiresAuth: true},
        children: [      
          { name: 'Post', props: true, path: ':slug', component: () => import('pages/Article.vue'), meta: {requiresAuth: true}}
        ]
      },
      { name: 'Profile', props: true, path: '5m/:slug', component: () => import('pages/Profile.vue'), meta: {requiresAuth: true},
        children: [
          { name: 'Settings', props: true, path: 'settings', component: () => import('pages/Settings.vue'), meta: {requiresAuth: true}},
        ]
      }, 
      { name: 'Note', props: true, path: '5m/:slug/note', component: () => import('pages/Note.vue'), meta: {requiresAuth: true}},
      { name: 'Subscriptions', props: true, path: '5m/:slug/subscriptions', component: () => import('pages/Subscription.vue'), meta: {requiresAuth: true}},
      { name: 'Subscribers', props: true, path: '5m/:slug/subscribers', component: () => import('pages/Subscription.vue'), meta: {requiresAuth: true}},
      { name: 'Subscribe', props: true, path: '5m/:slug/subscribe', component: () => import('pages/Subscription.vue'), meta: {requiresAuth: true}},
      { name: 'Chat', props: true, path: 'chat', component: () => import('pages/Chat.vue'), meta: {requiresAuth: true}},
      { name: 'Message', props: true, path: 'message', component: () => import('pages/Message.vue'), meta: {requiresAuth: true}},
      { name: 'Forum', props: true, path: 'forum', component: () => import('pages/Forum.vue'), meta: {requiresAuth: true}},
      { name: 'Invoice', props: true, path: 'invoice', component: () => import('pages/Invoice.vue'), meta: {requiresAuth: true}},
      { name: 'Invoice-Payment', props: true, path: 'invoice/:TransID', component: () => import('pages/Invoice.vue'), meta: {requiresAuth: true}},
    ]
  },
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}



export default routes
