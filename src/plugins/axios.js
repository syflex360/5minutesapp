import axios from 'axios'
import { LocalStorage } from 'quasar'

export default ({ Vue }) => {
  Vue.prototype.$axios = axios
  axios.defaults.headers.common['Authorization'] = LocalStorage.get.item('5minutes-token')
}