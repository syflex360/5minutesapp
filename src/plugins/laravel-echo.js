import Echo from 'laravel-echo'

// window.io = require('socket.io-client')

window.io = require('pusher-js')

// var host = 'localhost:6001';

const echo = new Echo ({   
    broadcaster: 'pusher',
    key: 'a09f9e3a4e7e6634e3d2',
    cluster: 'mt1',
    encrypted: true,
    // broadcaster: 'socket.io',
    // host: host,
})

export default ({Vue}) => {
    Vue.prototype.$echo = echo
}