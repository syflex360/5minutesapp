import pusher from 'pusher-js';

// leave the export, even if you don't use it
export default ({ app, router, Vue }) => {
  Vue.use(pusher)
}