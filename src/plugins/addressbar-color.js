import { AddressbarColor } from 'quasar'

// leave the export, even if you don't use it
export default ({ app, router, Vue }) => {
  Vue.use(AddressbarColor)
  AddressbarColor.set('#a2e3fa')
}
