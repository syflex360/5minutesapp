import home from './partials/home'
import navigation from './partials/navigation'
// This is just an example,
// so you can safely delete all default props below

export default {
  home,
  navigation
}
